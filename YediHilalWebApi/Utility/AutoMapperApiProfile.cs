﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YediHilalWebApi.Models;

namespace YediHilalWebApi.Utility
{
    public class AutoMapperApiProfile : AutoMapper.Profile
    {

        public AutoMapperApiProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<Menu,MenuDto>();
            CreateMap<Meeting, MeetingListDto>().ForMember(destination => destination.MeetingTypeName,  opts => opts.MapFrom(source => source.MeetingType.Name));

        }

        public static void Run()
        {
            AutoMapper.Mapper.Initialize(a =>
            {
                a.AddProfile<AutoMapperApiProfile>();
            });
        }

    }
}