﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace YediHilalWebApi.Models
{
    [Table("User", Schema = "Auth")]
    public class User
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public Person Person { get; set; }

    }

    [Table("Person", Schema = "Person")]
    public class Person
    {
        [Key]
        public int PersonId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string MobileNumber { get; set; }
        public string HomeAddress { get; set; }
        public string OfficeAddress { get; set; }
        public string ImageFile { get; set; }

        [ForeignKey("Title")]
        public int TitleId { get; set; }
        public Title Title { get; set; }


    }

    [Table("Title", Schema = "Auth")]
    public class Title
    {

        public int TitleId { get; set; }
        public string TitleName { get; set; }

        public ICollection<Person> Person { get; set; }
    }

    [Table("TitleMenu", Schema = "Auth")]
    public class TitleMenu
    {
        public int TitleMenuId { get; set; }
        public Title Title { get; set; }
        public Menu Menu { get; set; }
    }

    [Table("Menu", Schema = "Auth")]
    public class Menu
    {
        public int MenuId { get; set; }
        public string MenuName { get; set; }
        public string MenuPath { get; set; }
    }

    [Table("Meeting", Schema ="Dbo")]
    public class Meeting
    {
        public int MeetingId { get; set; }
        public string Title { get; set; }
        public MeetingType MeetingType { get; set; }
        public DateTime Date { get; set; }        
    }

    [Table("MeetingType", Schema ="Dbo")]
    public class MeetingType
    {
        public int MeetingTypeId { get; set; }
        public string Name { get; set; }

        public ICollection<Meeting> Meeting { get; set; }
    }


}