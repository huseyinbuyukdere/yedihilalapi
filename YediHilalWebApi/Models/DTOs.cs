﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YediHilalWebApi.Models
{

    public class UserDto
    {
        public UserDto(string username, string password)
        {
            Username = username;
            Password = password;
        }
        public int Id { get; set; }        
        public string Username { get; set; }
        public string Password { get; set; }        
    }

    public class MenuListDto
    {
        public List<MenuDto> MenuItems { get; set; }
    }

    public class MenuDto
    {
        public string MenuName { get; set; }
        public string MenuPath { get; set; }
    }


    public class MeetingListDto
    {
        public string MeetingId { get; set; }
        public string Title { get; set; }
        public string MeetingTypeName { get; set; }
        public DateTime Date { get; set; }
    }


}   