﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YediHilalWebApi.Models
{


    public class ApiManagerResponse
    {
        public ApiManagerResponse()
        {

        }

        public ApiManagerResponse(bool isOk,string desc,object data)
        {
            this.isOk= isOk;
            this.description = desc;
            this.data = data;
        }

        public bool isOk { get; set; }
        public string description { get; set; }
        public object data { get; set; }
    }

}