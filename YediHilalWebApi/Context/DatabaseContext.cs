﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using YediHilalWebApi.Models;

namespace YediHilalWebApi.Context
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext() : base("LocalDbConnection") { }

        public DbSet<Person> Persons { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Title> Titles { get; set; }
        public DbSet<TitleMenu> TitleMenus { get; set; }
        public DbSet<Menu> Menus { get; set; }
        public DbSet<Meeting> Meetings { get; set; }
        public DbSet<MeetingType> MeetingTypes { get; set; }
    }
}