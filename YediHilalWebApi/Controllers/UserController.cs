﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using YediHilalWebApi.Context;
using YediHilalWebApi.Models;

namespace YediHilalWebApi.Controllers
{
    public class UserController : ApiController
    {
        [HttpPost]
        [Route("user/control")]
        public HttpResponseMessage UserControl([FromBody] UserDto userDto)
        {
            ApiManagerResponse apiManagerResponse;
            var user = GetUserFromDb(userDto);
            if (user != null)
            {
                userDto = AutoMapper.Mapper.Map(user, userDto);
                apiManagerResponse = new ApiManagerResponse(true, "Giriş Başarılı", userDto);
            }
            else
            {
                apiManagerResponse= new ApiManagerResponse(false, "Giriş Başarısız", user);                
            }
            return Request.CreateResponse(HttpStatusCode.OK, apiManagerResponse);

        }

        public User GetUserFromDb(UserDto userDto)
        {
            User user = null;
            using (DatabaseContext dbContext = new DatabaseContext())
            {
                user = dbContext.Users.Where(x => x.Username == userDto.Username && x.Password == userDto.Password).FirstOrDefault();
            }
            return user;
        }

        [HttpPost]
        [Route("user/menu")]
        public HttpResponseMessage MenuControl([FromBody] UserDto user)
        {
            var resultList = GetUserMenuFromDb(user);
            return Request.CreateResponse(HttpStatusCode.OK, new ApiManagerResponse(true, "", resultList));
        }

        public List<Menu> GetUserMenuFromDb(UserDto userDto)
        {
            List<Menu> list = null;
            using (DatabaseContext dbContext = new DatabaseContext())
            {
                var user = dbContext.Users.Include("Person").Where(a=> a.Username == userDto.Username && a.Password == userDto.Password).FirstOrDefault();
                list = dbContext.TitleMenus.Where(a => a.Title.TitleId == user.Person.TitleId).Select(a=> a.Menu).ToList();
            }
            return list;
        }
    }
}
