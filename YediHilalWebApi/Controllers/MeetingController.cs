﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using YediHilalWebApi.Context;
using YediHilalWebApi.Models;
using YediHilalWebApi.Utility;

namespace YediHilalWebApi.Controllers
{
    [AllowCrossSiteJson]
    public class MeetingController : ApiController
    {


        [HttpGet]
        [Route("meeting/getList")]
        public HttpResponseMessage GetListMeetings()
        {
            ApiManagerResponse apiManagerResponse = new ApiManagerResponse();         
            var listMeetingFromDb = new List<Meeting>();
            using (DatabaseContext dbContext = new DatabaseContext())//Singleton Pattern , Factory Pattern , Abstract Factory Pattern (Desing Patterns)
            {
                listMeetingFromDb = dbContext.Meetings.Include("MeetingType").Select(a => a).ToList();
            }
            apiManagerResponse.description = " her şey yolunda";
            apiManagerResponse.isOk = true;
            List<MeetingListDto> listMeetingApiResponse = new List<MeetingListDto>();
            apiManagerResponse.data =AutoMapper.Mapper.Map(listMeetingFromDb, listMeetingApiResponse);
            return Request.CreateResponse(HttpStatusCode.OK, apiManagerResponse);
        }



    }
}
